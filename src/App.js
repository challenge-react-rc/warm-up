import { useState } from "react";
import { Switch, Route, HashRouter } from "react-router-dom";
import Details from "./Components/Details/Details";
import Header from "./Components/Header/Header";
import Add from "./Components/Home/Add";
import Home from "./Components/Home/Home";
import Login from "./Components/Login/Login";
import Error404 from "./Components/Error404";
import Footer from "./Components/Footer/Footer";
import { AuthProvider } from "./Context/ContextAuth";
import "./App.css";

function App() {
  const [collectedData, setCollectedData] = useState([]);
  const [hello, setHello] = useState([]);

  return (
    <AuthProvider>
      <HashRouter>
        <Header />
        <Switch>
          <Route exact path="/details/:Id" component={Details} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/add">
            <Add
              hello={hello}
              collectedData={collectedData}
              setCollectedData={setCollectedData}
            />
          </Route>
          <Route exact path="/">
            <Home
              collectedData={collectedData}
              hello={hello}
              setHello={setHello}
            />
          </Route>
          <Route path="*" component={Error404} />
        </Switch>
        <Footer />
      </HashRouter>
    </AuthProvider>
  );
}

export default App;
