import { useState, createContext } from "react";

const AuthContext = createContext();

const initialAuth = false;

const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState(initialAuth);

  const handleAuth = () => {
    if (auth) {
      localStorage.removeItem("token");
      setAuth(false);
    } else if (localStorage.getItem("Token")) {
      setAuth(true);
    }
  };

  const data = { auth, handleAuth };

  return <AuthContext.Provider value={data} children={children} />;
};

export { AuthProvider };
export default AuthContext;
