import { useContext, useState } from "react";
import { Formik, Form as FormFormik, Field, ErrorMessage } from "formik";
import { Container, OverlayTrigger, Tooltip, Button } from "react-bootstrap";
import AuthContext from "../../Context/ContextAuth";
import { Redirect } from "react-router-dom";

const Login = () => {
  const { handleAuth, auth } = useContext(AuthContext);
  const [TOKEN, setTOKEN] = useState([]);

  // Button Login animation // Bootstrap //
  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      let's get started!
    </Tooltip>
  );

  if (localStorage.getItem("Token")) {
    return <Redirect to="/" />;
  }
  return (
    <Container className="vh-100 vw-100 d-flex justify-content-center flex-column">
      <h1 className=" text-center">Login</h1>
      <Formik
        initialValues={{ email: "challenge@alkemy.org", password: "react" }}
        validate={(values) => {
          const errors = {};
          if (!values.email) {
            errors.email = "Email is Required";
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          ) {
            errors.email = "Invalid email address";
          }
          if (!values.password) {
            errors.password = "Password is Required";
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            fetch("http://challenge-react.alkemy.org", {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
              },
              body: JSON.stringify(values),
            }).then((data) => {
              setTOKEN(data);
              handleAuth();
              localStorage.setItem("Token", TOKEN);
              console.log(TOKEN);
            });
            setSubmitting(false);
          }, 400);
        }}
      >
        {({ isSubmitting }) => (
          <FormFormik className="d-flex flex-column">
            <Field type="email" name="email" className="m-3" />
            <ErrorMessage name="email" component="div" />
            <Field type="password" name="password" className="m-3" />
            <ErrorMessage name="password" component="div" />
            <OverlayTrigger
              placement="bottom"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip}
            >
              {auth ? (
                <Button
                  variant="success"
                  type="submit"
                  disabled={isSubmitting}
                  className="m-3"
                >
                  Login
                </Button>
              ) : (
                <Button
                  variant="success"
                  type="submit"
                  disabled={isSubmitting}
                  className="m-3"
                >
                  Logout
                </Button>
              )}
            </OverlayTrigger>
          </FormFormik>
        )}
      </Formik>
    </Container>
  );
};

export default Login;
