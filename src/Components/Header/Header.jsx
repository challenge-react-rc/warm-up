import { Nav, Navbar, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <Navbar
      fixed
      collapseOnSelect
      expand="lg"
      bg="dark"
      variant="dark"
      className="border-bottom border-secondary"
    >
      <Container>
        <Navbar.Brand>Warm Up - Rodrigo Calderon</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto d-flex justify-content-end">
            <Nav>
              <Nav.Link>
                <Link
                  to="/"
                  className="text-decoration-none pointer-event link-light"
                >
                  Home
                </Link>
              </Nav.Link>
            </Nav>
            <Nav>
              <Nav.Link>
                <Link
                  to="/login"
                  className="text-decoration-none pointer-event link-light"
                >
                  Login
                </Link>
              </Nav.Link>
            </Nav>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
