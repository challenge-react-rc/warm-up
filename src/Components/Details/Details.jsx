import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Card, Container, Dropdown, Button } from "react-bootstrap";
import Error404 from "../Error404";
import Charging from "../Charging";

const Details = () => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [editing, setEditing] = useState(false);
  const [error, setError] = useState(null);
  const [hello, setHello] = useState({});
  const { Id } = useParams();
  const [newHello, setNewHello] = useState({
    id: "",
    title: "",
    body: "",
  });

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts/" + Id)
      .then((response) => response.json())
      .then(
        (response) => {
          setIsLoaded(true);
          setHello(response);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, [Id, setHello]);

  const selectHello = (e) => {
    setNewHello(e);
    if (editing) {
      if (hello.title.value === 0) {
        alert("All the text areas need to be completed");
      }
      setEditing(false);
    } else {
      setEditing(true);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewHello((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleDelete = (hello) => {};

  if (error) {
    return <Error404 />;
  } else if (!isLoaded) {
    return <Charging />;
  } else {
    return (
      <Container className="vh-100 d-flex flex-column justify-content-center text-center">
        <h1 className="mb-3">Details</h1>
        <Card className="bg-dark text-light shadow">
          <Card.Header>Title</Card.Header>
          <Card.Body>
            <blockquote className="blockquote mb-5 p-3">
              {editing ? (
                <textarea
                  required
                  name="title"
                  value={hello.title && newHello.title}
                  rows="1"
                  className="col-12 text-center"
                  onChange={handleChange}
                />
              ) : (
                <p>{hello.title}</p>
              )}
            </blockquote>
          </Card.Body>
          <Dropdown.Divider className="border-secondary mb-0" />
          <Card.Header>Body</Card.Header>
          <Card.Body>
            <blockquote className="blockquote mb-5 p-3">
              {editing ? (
                <textarea
                  required
                  name="body"
                  value={hello.body && newHello.body}
                  rows="4"
                  className="col-12 text-center"
                  onChange={handleChange}
                />
              ) : (
                <p>{hello.body}</p>
              )}
            </blockquote>
          </Card.Body>
        </Card>
        <Container>
          {!editing ? (
            <Button className="m-3 mt-5 w-25" onClick={selectHello}>
              Edit
            </Button>
          ) : (
            <Button className="m-3 mt-5 w-25" onClick={selectHello}>
              Save changes
            </Button>
          )}
          <Button
            className="btn-danger m-3 mt-5 w-25"
            onClick={handleDelete(hello)}
          >
            Delete
          </Button>
        </Container>
      </Container>
    );
  }
};

export default Details;
