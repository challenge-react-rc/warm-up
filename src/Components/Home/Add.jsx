import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Button, Container, Row } from "react-bootstrap";

const Add = ({ hello, collectedData, setCollectedData }) => {
  let history = useHistory();

  const handleSaveData = () => {
    if (collectedData.title.length === 0 || collectedData.body.length === 0) {
      alert(
        "Have not ideas? To save the data you need to complete all the inputs areas"
      );
      return;
    } else {
      console.log("data saved: ", collectedData);
      let path = "/";
      history.push(path);
    }
  };

  const handleChange = (e) => {
    setCollectedData({ ...collectedData, [e.target.name]: e.target.value });
    console.log(collectedData);
  };

  return (
    <Container className="vh-100  p-3">
      <h1>Add</h1>
      <Row>
        <h3>Title</h3>
        <input type="text" name="title" onChange={handleChange} />
        <h3 className="mt-3">Body</h3>
        <input type="text" name="body" onChange={handleChange} />
        <Container className="d-flex justify-content-evenly p-0">
          <div>
            <span className="mt-5">Id: </span>
            <input
              disabled
              type="text"
              name="id"
              value={hello.length + 1}
              className="mt-5"
            />
          </div>
          <Button className="w-25 mt-5" onClick={handleSaveData}>
            Saved
          </Button>
        </Container>
      </Row>
    </Container>
  );
};

export default Add;
