import { useEffect, useState, useContext } from "react";
import { Container, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import Table from "./Table";
import Error404 from "../Error404";
import Charging from "../Charging";
import Controller from "./Controller";
import AuthContext from "../../Context/ContextAuth";

const Home = ({ collectedData, hello, setHello }) => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [error, setError] = useState(null);
  const [init, setInit] = useState(0);
  const [end, setEnd] = useState(10);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then(
        (response) => {
          setIsLoaded(true);
          setHello(response);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, [setHello]);

  if (error) {
    return <Error404 />;
  } else if (!isLoaded) {
    return <Charging />;
  } else if (localStorage.getItem("Token") === 0) {
    return <Redirect to="/login" />;
  } else {
    return (
      <Container className="vh-100 d-flex flex-column justify-content-center text-center">
        <Container className="d-flex justify-content-between p-3">
          <h1>Home</h1>
          <Button className="btn-success p-0">
            <Link
              to="/add"
              className="text-decoration-none text-dark fw-bolder p-3"
            >
              Add new post
            </Link>
          </Button>
        </Container>
        <Table
          hello={hello}
          init={init}
          end={end}
          collectedData={collectedData}
        />
        <Controller
          hello={hello}
          init={init}
          setInit={setInit}
          end={end}
          setEnd={setEnd}
        />
      </Container>
    );
  }
};

export default Home;
