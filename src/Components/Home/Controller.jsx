import { Button, Container } from "react-bootstrap";

const Controller = ({ hello, init, setInit, end, setEnd }) => {
  const handlePrev = () => {
    setInit(init - 10);
    setEnd(end - 10);
  };
  const handleNext = () => {
    setInit(init + 10);
    setEnd(end + 10);
  };

  return (
    <Container>
      {init === 0 ? (
        <Button disabled className="m-2">
          Previus
        </Button>
      ) : (
        <Button className="m-2" onClick={() => handlePrev()}>
          Previus
        </Button>
      )}

      <span>{`${init === 0 ? "1" : init + 1} - ${
        end > hello.length ? hello.length : end
      }`}</span>

      {end >= hello.length ? (
        <Button disabled className="m-2">
          Next
        </Button>
      ) : (
        <Button className="m-2" onClick={() => handleNext()}>
          Next
        </Button>
      )}
    </Container>
  );
};

export default Controller;
