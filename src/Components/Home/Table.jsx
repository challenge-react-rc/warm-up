import { Table, Button, Dropdown, ButtonGroup } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import React, { useEffect } from "react";

const Tables = ({ hello, init, end, collectedData }) => {
  const history = useHistory();

  const setDataToEdit = () => {};
  const deleteData = () => {
    hello.shift(hello);
  };

  useEffect(() => {
    hello.unshift(collectedData);
  }, [hello]);

  return (
    <Table responsive striped bordered hover variant="dark">
      <thead>
        <tr>
          <th>Id</th>
          <th>Title</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody style={{ textAlign: "left" }}>
        {hello.slice(init, end).map((hello) => (
          <tr key={hello.id}>
            <th style={{ textAlign: "center" }}>{hello.id}</th>
            <th>{hello.title}</th>
            <th style={{ textAlign: "center" }}>
              <Dropdown as={ButtonGroup}>
                <Button
                  variant="secondary"
                  onClick={() => history.push("/details/" + hello.id)}
                >
                  Details
                </Button>
                <Dropdown.Toggle
                  split
                  variant="secondary"
                  id="dropdown-split-basic"
                />
                <Dropdown.Menu variant="dark" className="p-0">
                  <Dropdown.Item onClick={() => deleteData(hello)}>
                    Delete
                  </Dropdown.Item>
                  <Dropdown.Item onClick={() => setDataToEdit(hello)}>
                    <Link
                      className="w-100 text-decoration-none text-white"
                      to={"/details/" + hello.id}
                    >
                      Edit
                    </Link>
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </th>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default Tables;
