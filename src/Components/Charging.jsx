import { Container } from "react-bootstrap";
import styles from "./Charging.module.css";

const Charging = () => {
  return (
    <Container className="bg-dark vh-100 vw-100 d-flex justify-content-center align-items-center">
      <h1>Loading...</h1>
      <div className={styles.ldsSpinner}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </Container>
  );
};

export default Charging;
