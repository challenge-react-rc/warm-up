import { Container, Row, Col } from "react-bootstrap";

const Footer = () => {
  return (
    <Container fluid className="border-top border-secondary">
      <Row className="justify-content-md-center text-center p-5">
        <Col xs lg="3">
          Copyright 2021
        </Col>
        <Col md="auto">Hecho con 🖤 por R. Calderon</Col>
        <Col xs lg="3">
          Redes sociales
        </Col>
      </Row>
    </Container>
  );
};

export default Footer;
